# WeatherApp

Simple app for searching weather by city. 

`npm start` - run a dev server on `http://localhost:4200/`

`npm run lint` - run ts linter.

`npm run build dev` - generate dev. build.

`npm run build prod` - generate prod. build.

`npm run test` - run unit tests.

### About charts

For displaying charts used [Highcharts](https://www.highcharts.com/) library.

Each charts in app can be configured. Charts configs stored in:

`highcharts-viewer.settings.ts`

### About e2e and unit-test

There are a lot of work to be done on the tests. It will take a long time, so I decided to do it at another time.

### About linter

Linter succeeded.

All settings, you can see in the file `tslint.json`

### About open weather map api

Forecast for 16 days is available only for paid accounts. So i used 5 days forecast api.
