/**
 * Weather details viewer
 */
import { Component, Input } from '@angular/core';
import { WeatherResponse } from '../../classes';
import { IWeatherListItem } from '../../types';

@Component({
  selector: 'app-weather-details-viewer',
  templateUrl: './weather-details-viewer.component.html',
  styleUrls: ['./weather-details-viewer.component.scss']
})
export class WeatherDetailsViewerComponent  {

  @Input()
  public weatherInfo: WeatherResponse;

  public get actualWeatherData(): IWeatherListItem {
    return this.weatherInfo ? this.weatherInfo.getActualWeatherListItem() : <IWeatherListItem>{};
  }

  constructor() {
  }
}
