/**
 * Temperature viewer component
 */
import { Component, Input } from '@angular/core';
import { WeatherResponse } from '../../classes';
import { IWeather, IWeatherListItem } from '../../types';

@Component({
  selector: 'app-temperature-viewer',
  templateUrl: './temperature-viewer.component.html',
  styleUrls: ['./temperature-viewer.component.scss']
})
export class TemperatureViewerComponent {

  @Input()
  public weatherInfo: WeatherResponse;

  public get actualWeatherData(): IWeatherListItem {
    return this.weatherInfo ? this.weatherInfo.getActualWeatherListItem() : <IWeatherListItem>{};
  }

  constructor() { }
}
