/**
 * HighchartViewerComponent unit tests
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighchartViewerComponent } from './highchart-viewer.component';

describe('HighchartViewerComponent', () => {
  let component: HighchartViewerComponent;
  let fixture: ComponentFixture<HighchartViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighchartViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighchartViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
