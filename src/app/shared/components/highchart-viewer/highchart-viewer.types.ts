/**
 * HighchartViewerComponent types
 */
export enum ChartType {
  LINE = 'line',
  COLUMN = 'column',
}
