/**
 * Highchart viewer component
 */
import { AfterContentInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import { chart, ChartObject, Options } from 'highcharts';

@Component({
  selector: 'app-highchart-viewer',
  templateUrl: './highchart-viewer.component.html',
  styleUrls: ['./highchart-viewer.component.scss']
})
export class HighchartViewerComponent implements AfterContentInit {

  @Input()
  public chartOptions: Options;

  @ViewChild('chartContainer')
  public container: ElementRef;

  private chart: ChartObject;

  constructor() {
  }

  public ngAfterContentInit(): void {
    this.createChart();
  }

  private createChart(): void {
    this.chart = chart(this.container.nativeElement, this.chartOptions);
  }

}
