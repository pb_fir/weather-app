/**
 * Highcharts default and specific configs for charts
 */
import { Options } from 'highcharts';
import { ChartType } from './highchart-viewer.types';

export const highchartDefaultConfig: Options = {
  chart: {
    type: ChartType.LINE,
  },
  xAxis: {
    categories: [],
  },
  yAxis: {
    title: {
      text: '',
    }
  },
  legend: {
    enabled: false,
  },
  title: {
    text: '',
  },
  plotOptions: {
    line: {
      dataLabels: {
        enabled: true,
      },
      enableMouseTracking: false,
    }
  },
  series: [],
};

export const temperatureChartOptions: Options = {
  chart: {
    type: ChartType.LINE,
  },
};

export const windChartOptions: Options = {
  chart: {
    type: ChartType.COLUMN,
  },
};

export const presureChartOptions: Options = {
  chart: {
    type: ChartType.LINE,
  },
};

export const humidityChartOptions: Options = {
  chart: {
    type: ChartType.LINE,
  },
};
