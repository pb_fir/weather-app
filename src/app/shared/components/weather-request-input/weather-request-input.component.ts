/**
 * Weather request input component
 */
import { Component, EventEmitter, Output } from '@angular/core';
import { debounce } from '../../decorators';

@Component({
  selector: 'app-weather-request-input',
  templateUrl: './weather-request-input.component.html',
  styleUrls: ['./weather-request-input.component.scss']
})
export class WeatherRequestInputComponent {

  @Output()
  public weatherRequest: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  @debounce(1500)
  public onKeyUp(request: string = ''): void {
      this.weatherRequest.next(request);
  }
}
