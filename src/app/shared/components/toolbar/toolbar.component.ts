/**
 * Toolbar component
 */
import { Component } from '@angular/core';
import { OpenWeatherIcon } from '../open-weather-icon';
import { WeatherInfoService } from '../../services/weather-info';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent {

  public openWeatherIcon = OpenWeatherIcon;

  constructor(private weatherInfoService: WeatherInfoService) {
  }

  public setWeatherRequest(request: string): void {
    this.weatherInfoService.weatherRequest$.next(request);
  }
}
