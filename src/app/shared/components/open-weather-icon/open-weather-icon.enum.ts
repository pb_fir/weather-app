/**
 * Open weather icons enum
 */
export enum OpenWeatherIcon {
  CLEAR_SKY = '01d',
  CLEAR_SKY_NIGHT = '01n',
  FEW_CLOUDS = '02d',
  FEW_CLOUDS_NIGHT = '02n',
  SCATTERED_CLOUDS = '03d',
  SCATTERED_CLOUDS_NIGHT = '03n',
  BROKEN_CLOUDS = '04d',
  BROKEN_CLOUDS_NIGHT = '04n',
  SHOWER_RAIN = '09d',
  SHOWER_RAIN_NIGHT = '09n',
  RAIN = '10d',
  RAIN_NIGHT = '10n',
  THUNDERSTORM = '11d',
  THUNDERSTORM_NIGHT = '11n',
  SNOW = '13d',
  SNOW_NIGHT = '13n',
  MIST = '50d',
  MIST_NIGHT = '50n'
}
