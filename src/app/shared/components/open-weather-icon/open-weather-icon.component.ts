/**
 * Open weather icon component
 */
import { Component, Input } from '@angular/core';
import { OpenWeatherIcon } from './open-weather-icon.enum';

@Component({
  selector: 'app-open-weather-icon',
  templateUrl: './open-weather-icon.component.html',
  styleUrls: ['./open-weather-icon.component.scss']
})
export class OpenWeatherIconComponent {

  public icon: string;

  @Input()
  public set iconCode(icon: OpenWeatherIcon | string) {
    const iconPrefix = 'owi-';
    this.icon = `${iconPrefix}${icon}`;
  }

  constructor() {
  }
}
