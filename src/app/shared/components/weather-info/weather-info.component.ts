/**
 * Weather info component
 */
import { Component } from '@angular/core';
import { HttpActivityService, WeatherInfoService } from '../../services';
import { WeatherResponse } from '../../classes';
import { BaseComponent } from '../base-component/base.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.scss']
})
export class WeatherInfoComponent extends BaseComponent {

  public weatherInfo: WeatherResponse;
  public weatherRequest: string = '';
  public httpIsActive: boolean = false;

  constructor(private weatherInfoService: WeatherInfoService,
              private httpActivityService: HttpActivityService) {
    super();
  }

  public ngOnInit() {
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    this.weatherInfoService.weatherRequest$
      .pipe(takeUntil(this.dispose$))
      .subscribe((request: string) => {
        this.weatherRequest = request;
      });

    this.httpActivityService.httpActivityStatus$
      .pipe(takeUntil(this.dispose$))
      .subscribe((httpIsActive: boolean) => {
        this.httpIsActive = httpIsActive;
      });

    this.weatherInfoService.weatherInfo$
      .pipe(takeUntil(this.dispose$))
      .subscribe((weatherInfo: WeatherResponse) => {
        this.weatherInfo = weatherInfo;
      });
  }
}
