/**
 * WeatherTabsViewerComponent unit tests
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherTabsViewerComponent } from './weather-tabs-viewer.component';

describe('WeatherTabsViewerComponent', () => {
  let component: WeatherTabsViewerComponent;
  let fixture: ComponentFixture<WeatherTabsViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherTabsViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherTabsViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
