/**
 * Weather tabs viewer component
 */
import { Component, Input, OnInit } from '@angular/core';
import { WeatherResponse } from '../../classes';
import { IChartData } from '../../types';
import { Options } from 'highcharts';
import {
  highchartDefaultConfig,
  humidityChartOptions,
  presureChartOptions,
  temperatureChartOptions,
  windChartOptions
} from '../highchart-viewer';

@Component({
  selector: 'app-weather-tabs-viewer',
  templateUrl: './weather-tabs-viewer.component.html',
  styleUrls: ['./weather-tabs-viewer.component.scss']
})
export class WeatherTabsViewerComponent implements OnInit {

  @Input()
  public weatherInfo: WeatherResponse;

  public temperatureChartOptions: Options;
  public windChartOptions: Options;
  public presureChartOptions: Options;
  public humidityChartOptions: Options;

  private temperatureData: IChartData[];
  private windData: IChartData[];
  private presureData: IChartData[];
  private humidityData: IChartData[];

  constructor() {
  }

  public ngOnInit(): void {
    this.initChartsData();
    this.initChartsOptions();
  }

  private initChartsData(): void {
    this.temperatureData = this.weatherInfo.getTemperatureDataFromList();
    this.windData = this.weatherInfo.getWindDataFromList();
    this.presureData = this.weatherInfo.getPresureDataFromList();
    this.humidityData = this.weatherInfo.getHumidityDataFromList();
  }

  private initChartsOptions(): void {
    this.temperatureChartOptions = this.generateChartConfig(this.temperatureData, temperatureChartOptions);
    this.windChartOptions = this.generateChartConfig(this.windData, windChartOptions);
    this.presureChartOptions = this.generateChartConfig(this.presureData, presureChartOptions);
    this.humidityChartOptions = this.generateChartConfig(this.humidityData, humidityChartOptions);
  }

  private generateChartConfig(chartData: IChartData[] = [], specificOptions: Options = {}): Options {
    return {
      ...highchartDefaultConfig,
      ...specificOptions,
      xAxis: {
        categories: chartData.map(cd => cd.date),
      },
      series: [{
        data: chartData.map(cd => cd.value),
      }]
    };
  }
}
