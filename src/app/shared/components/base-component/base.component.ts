/**
 * Base component class
 */
import { OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';

export abstract class BaseComponent implements OnDestroy {

  public readonly dispose$: Subject<void> = new Subject<void>();

  public ngOnDestroy() {
    this.dispose$.next();
    this.dispose$.complete();
  }
}
