/**
 * Debounce decorator
 */
import { throttle } from 'lodash';

const DEFAULT_DELAY_MS = 300;

export function debounce(milliseconds: number = DEFAULT_DELAY_MS): MethodDecorator {
  return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
    const original = descriptor.value;

    descriptor.value = throttle(original, milliseconds);

    return descriptor;
  };
}
