/**
 * Open weather map api types
 * @see https://openweathermap.org/api
 */

export interface IWeather {
  description: string;
  icon: string;
  id: number;
  main: string;
}

export interface ICity {
  country: string;
  geoname_id: number;
  iso2: string;
  lat: number;
  lon: number;
  name: string;
  population: number;
  ['type']: string;
}

export interface IClouds {
  all: number;
}

export interface IWind {
  speed: number;
  deg: number;
}

export interface IMainWeatherData {
  grnd_level: number;
  humidity: number;
  pressure: number;
  sea_level: number;
  temp: number;
  temp_kf: number;
  temp_max: number;
  temp_min: number;
}

export interface ISysWeatherData {
  pod?: number;
}

export interface IWeatherListItem {
  clouds: IClouds;
  deg: number;
  dt: number;
  dt_txt: string;
  main: IMainWeatherData;
  sys: ISysWeatherData;
  weather: IWeather[];
  wind: IWind;
}

export interface IWeatherResponse {
  city: ICity;
  cnt: number;
  cod: number;
  list: IWeatherListItem[];
  message: number;
}

