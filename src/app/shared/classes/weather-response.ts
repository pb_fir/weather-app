/**
 * Class contains logic for working with weather response
 */
import {
  IChartData,
  ICity,
  IWeatherListItem,
  IWeatherResponse
} from '../types';
import { MomentService } from '../services/moment';
import { isEmpty, get } from 'lodash';

export class WeatherResponse implements IWeatherResponse {

  public city: ICity;
  public cnt: number;
  public cod: number;
  public list: IWeatherListItem[];
  public message: number;
  private actualListItem: IWeatherListItem;

  constructor({city, cnt, cod, list = [], message}: IWeatherResponse) {
    this.city = city;
    this.cnt = cnt;
    this.cod = cod;
    this.list = list;
    this.message = message;
  }

  public getTemperatureDataFromList(): IChartData[] {
    return this.list.map(({dt_txt, main}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(main, 'temp', 0),
      };
    });
  }

  public getWindDataFromList(): IChartData[] {
    return this.list.map(({dt_txt, wind}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(wind, 'speed', 0),
      };
    });
  }

  public getPresureDataFromList(): IChartData[] {
    return this.list.map(({dt_txt, main}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(main, 'pressure', 0),
      };
    });
  }

  public getHumidityDataFromList(): IChartData[] {
    return this.list.map(({main, dt_txt}: IWeatherListItem) => {
      const shortDate = MomentService.getShortDateDescr(dt_txt);

      return {
        date: shortDate,
        value: get(main, 'humidity', 0),
      };
    });
  }

  public getActualWeatherListItem(): IWeatherListItem {
    if (isEmpty(this.actualListItem)) {
      this.actualListItem = this.list.find((listItem: IWeatherListItem) => MomentService.isCurrentDate(listItem.dt_txt));
    }
    return this.actualListItem;
  }
}
