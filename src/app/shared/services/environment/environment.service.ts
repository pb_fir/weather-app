/**
 * Environment service
 */
import { Injectable } from '@angular/core';
import { ENVIRONMENT } from './environment.enum';
import { environment } from '@environment/environment';
import { get } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {

  constructor() {
  }

  public getVariable(variable: ENVIRONMENT): ENVIRONMENT {
    return get(environment, variable, null);
  }
}
