/**
 * Http activity interceptor
 */
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpActivityService } from '../http-activity';
import { Observable } from 'rxjs';
import { delay, finalize, tap } from 'rxjs/operators';

@Injectable()
export class HttpActivityInterceptor implements HttpInterceptor {

  constructor(private httpActivityService: HttpActivityService) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap(() => {
        this.httpActivityService.setHttpActivityStatus(true);
      }),
      finalize(() => {
          this.httpActivityService.setHttpActivityStatus(false);
      })
    );
  }
}
