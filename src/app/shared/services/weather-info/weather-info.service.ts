/**
 * Weather info service
 * @todo Need to provide using ngrx store instead current service on app growing
 */
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { WeatherResponse } from '../../classes';
import { debounceTime, distinctUntilChanged, filter, flatMap } from 'rxjs/operators';
import { OpenWeatherMapService } from '../open-weather-map';
import { isEmpty } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class WeatherInfoService {

  public weatherRequest$: Subject<string> = new Subject<string>();
  public weatherInfo$: Observable<WeatherResponse>;

  constructor(private opewWeatherMapService: OpenWeatherMapService) {
    this.initWeatherInfo();
  }

  private initWeatherInfo(): void {
    this.weatherInfo$ = this.weatherRequest$
      .pipe(
        filter(request => !isEmpty(request.trim())),
        debounceTime(300),
        distinctUntilChanged(),
        flatMap(request => this.opewWeatherMapService.getWeatherByRequest(request))
      );
  }

}
