/**
 * Moment service
 */
import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
import { Moment } from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class MomentService {

  /**
   * Get name of local timezone
   * @returns {string} name of local timezone
   */
  public static localTimezone: string = moment.tz.guess();

  /**
   * Set timezone for moment object
   * @returns {Moment} object with predefined local timezone
   */
  public static moment(...args: any[]): any {
    const localTimeZone: string = MomentService.localTimezone;

    // @ts-ignore
    return moment.tz(...args, localTimeZone);
  }

  /**
   * Return date in format DD MMM
   * @param timestamp - timestamp to format
   */
  public static getShortDateDescr(time: string): string {
      const FORMAT = 'DD MMM';
      return MomentService.moment(time).format(FORMAT);
  }

  /**
   * Return date in format dddd, D MMMM
   * @param timestamp - timestamp to format
   */
  public static getDateDescrWithDayOfWeek(timestamp?: number): string {
    const FORMAT = 'dddd, D MMMM';
    return MomentService.moment(timestamp).format(FORMAT);
  }

  /**
   * Checks if current timestamp is current day
   * @param timestamp
   */
  public static isCurrentDate(time: string): boolean {
    const currentDate = MomentService.moment();
    const dateToCheck = MomentService.moment(time);
    return currentDate.isSame(dateToCheck, 'day');
  }

  constructor() { }
}
