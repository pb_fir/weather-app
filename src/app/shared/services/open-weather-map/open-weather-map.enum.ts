/**
 * Open weather map api enums.
 */
export enum OPER_WEATHER_QUERY_PARAMS {
  QUERY = 'q',
  CITY_ID = 'id',
  COUNT = 'cnt',
  ZIP = 'zip',
  APP_ID = 'APPID',
  TYPE = 'type',
  SORT = 'sort',
  UNITS = 'units'
}

export enum UNITS {
  IMPERIAL = 'imperial',
  METRIC = 'metric',
}
